package de.ninopeters.htmxtrial

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HtmxTrialApplication

fun main(args: Array<String>) {
    runApplication<HtmxTrialApplication>(*args)
}
