package de.ninopeters.htmxtrial

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
@RequestMapping(value = ["/v1/counter"])
class CounterController(val counterService: CounterService) {

    @GetMapping
    fun counter(): ModelAndView {
        val mv = ModelAndView()
        mv.viewName = "components/counter"
        mv.model["count"] = counterService.increment()

        return mv
    }
}