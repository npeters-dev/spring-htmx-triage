package de.ninopeters.htmxtrial

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView


@Controller
@RequestMapping
class PageController(val counterService: CounterService) {

    @GetMapping
    fun index(): ModelAndView {
        val mv = ModelAndView()
        mv.viewName = "pages/index"
        mv.model["title"] = "Hello World"
        mv.model["count"] = counterService.count()

        return mv
    }

    @GetMapping("/test")
    fun test(): ModelAndView {
        val mv = ModelAndView()
        mv.viewName = "pages/test"
        mv.model["title"] = "Test"
        mv.model["count"] = counterService.count()

        return mv
    }
}
